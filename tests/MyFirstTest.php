<?php

//require __DIR__ . '/Area.php';
use SON\Area;

class MyFirstTest extends \PHPUnit\Framework\TestCase
{
    public function testArray() {
        $array = ['well'];
        $this->assertNotEmpty($array);
    }


    public function testArea(){
        $area = new Area();
        $this->assertEquals(6,$area->getArea(2,3));
    }

}